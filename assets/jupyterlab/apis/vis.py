"""
This package contains all functions related to visualizations.
"""
import matplotlib.pyplot as plt
import pandas as pd

class Pie:
    @staticmethod
    def render_pie(planning_plants, result, roundTo, unit, location):
        """
        Returns the simple pie chart

        Parameters
        ----------

        planning_plants
            a list of planning plants for creating subplots
        result
            a dataset
        roundTo
            decimal place rounding
        unit
            desired unit (if numbers were to be in millions, 1000000)
        location
            location of the legend
        """
        print ("Sample")

class Bar:
    @staticmethod
    def render_stacked_bar(planning_plants, result, roundTo, unit, location):
        """
        Returns the stacked bar chart

        Parameters
        ----------

        planning_plants
            a list of planning plants for creating subplots
        result
            a dataset
        roundTo
            decimal place rounding
        unit
            desired unit (if numbers were to be in millions, 1000000)
        location
            location of the legend
        """
        fig, axes = plt.subplots(nrows = 1, ncols = planning_plants.size, sharey=False, figsize=(20,10))
        #For each planning plant
        for i, item in enumerate(planning_plants):
            #Prepare a new dataframe with rows having only that particular planning plant
            new_df = result.loc[result["planning_plant"] == item]

            #Then get the sum of actual costs per (work order type and fiscal year)
            result_int = new_df.groupby(['work_order_type', 'fiscal_year'])['actual_costs'].agg('sum').reset_index()

            #After extracting unique set of work order type and fiscal year
            work_order_type_uniques = sorted(new_df['work_order_type'].unique())
            fiscal_year_unique = sorted(new_df['fiscal_year'].unique().astype(int))

            #Prepare a new dataframe that will be used as an input for a stacked bar chart 
            #(e.g. {'PM01': ['total_actual_costs_year_1991', 'total_actual_costs_year_1992']})
            frame_dict = {}
            for wo in work_order_type_uniques:
                result_tmp = result_int.loc[result_int['work_order_type']==wo]
                tmp_list = []
                for fy in fiscal_year_unique:
                    result_fn = result_tmp.loc[result_tmp['fiscal_year'].astype(int)==fy]
                    if(result_fn['actual_costs'].empty):
                        tmp_list.append(0.1)
                    else:
                        tmp = result_fn['actual_costs'].values[0].astype(float)/unit
                        tmp_list.append(tmp.round(roundTo))
                frame_dict[wo] = tmp_list           
            df = pd.DataFrame(frame_dict, index=fiscal_year_unique)

            #Compute total actual costs per fiscal year
            df['total_costs'] = df[list(df.columns)].sum(axis=1)
            df_total = df['total_costs'].round(roundTo)

            #Prepare labelling for the chart and render
            df = df.iloc[:,0:len(work_order_type_uniques)]
            the_chart = df.plot.bar(stacked=True, ax=axes[i])
            axes[i].set(xlabel="Fiscal Year", ylabel="Total Actual Costs (in mil.)")
            axes[i].title.set_text(item)
            axes[i].legend(loc = location)
            #Computation for total labeling per stacked bar
            df_rel = df[df.columns[1:]].div(df_total, 0).round(roundTo)*100
            for n in df_rel:
                for i, (cs, ab, pc, tot) in enumerate(zip(df.iloc[:, 1:].cumsum(1)[n], df[n], df_rel[n], df_total)):
                    the_chart.text(i - 0.2, tot + 5, str(tot), va='center')
    @staticmethod
    def render_bar(planning_plants, result, roundTo, unit, location):
        """
        Returns the simple bar chart

        Parameters
        ----------

        planning_plants
            a list of planning plants for creating subplots
        result
            a dataset
        roundTo
            decimal place rounding
        unit
            desired unit (if numbers were to be in millions, 1000000)
        location
            location of the legend
        """
        print ("Sample")